# Сборка 100 иконок социальных сетей

Все иконки в разных форматах:  **svg, png, eps** и с разными стилями **Цветные, Черные, Белые, Минималистические без круга**. 
_______
## Цветные
![Цветные вариант](https://gitlab.com/gp-ttcsoft/source/100-social-icons/raw/master/Social_icons.png)
## Без кругов
![Без кругов](https://gitlab.com/gp-ttcsoft/source/100-social-icons/raw/master/Social_icons_no_circle.png)
## Черные
![Черно-белый вариант](https://gitlab.com/gp-ttcsoft/source/100-social-icons/raw/master/Social_icons_black.png)
## Белые
![Белые](https://gitlab.com/gp-ttcsoft/source/100-social-icons/raw/master/Social_icons_white.png)
